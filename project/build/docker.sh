#! /bin/bash

# ./docker.sh [profile] [version] [build_path] [jvm|native]
# ./docker.sh develop 1.0.0 flower-api native

build_root="../.."
build_prof=${1-"develop"}
build_path="${build_root}/${3-"flower-webapi"}"
build_file="webapi-runner.jar"
build_version=${2-"1.0.0"}

echo "install start....."
mvn clean install -Dmaven.test.skip=true -Dquarkus.profile=${build_prof} -f ${build_root}/pom.xml
mvn clean package -Dmaven.test.skip=true -Dquarkus.profile=${build_prof} -Dquarkus.package.type=native -f ${build_path}/pom.xml
echo "install finish"

echo "--docker build--"
cd ../docker/

echo "docker jar copy....."
cp ${build_path}/target/${build_file} ${build_file}

echo "docker build"
docker build -t flower/webapi:${build_version} -f Dockerfile.jvm .

echo "docker jar remove"
rm ${build_file}
echo "--docker build end--"

echo "--docker image--"
# save docker image
# docker save ${image_name}:${build_numb} > ${image_file}.tar

# copy docker image
# scp ${image_file}.tar root@${build_node}:/usr/local/webapi
echo "--docker image end--"

# run image
# docker load -i [file.tar]
# docker run -id --name [name] -p 8080:8080 [-e "JAVA_OPTS=-Dsummer.generator.worker-id=0"] -d flower/webapi:{version}

