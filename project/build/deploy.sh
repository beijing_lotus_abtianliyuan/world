#! /bin/bash

# ./deploy.sh [profile] [origin_path] [target_path]
# ./deploy.sh develop flower-api webapi-9000

build_root="../.."
build_prof=${1-"develop"}
build_path="${build_root}/${2-"flower-webapi"}"
build_file="webapi-runner.jar"

remote_host="root@101.200.53.244"
remote_path="/usr/local/${3-"webapi-9000"}"

echo "[${build_prof}] install start....."
mvn clean install -Dmaven.test.skip=true -Dquarkus.profile=${build_prof} -f ${build_root}/pom.xml
mvn clean package -Dmaven.test.skip=true -Dquarkus.profile=${build_prof} -Dquarkus.package.type=uber-jar -f ${build_path}/pom.xml

echo "[${build_prof}] copy start....."
scp ${build_path}/target/${build_file} ${remote_host}:${remote_path}/app.jar

echo "[${build_prof}] deploy start....."
ssh -t -t ${remote_host} << EOF
    echo "remote deploy path....."
    cd ${remote_path}

    echo "kill jar application..."
    ps -ef | grep 'app.jar' | grep -v grep | awk '{print $2}' | xargs kill -9

    echo "start jar application..."
    /usr/local/share/jdk-17.0.1/bin/java -jar app.jar&
    echo "start jar application"

    exit
EOF

echo "[${build_prof}] deploy finish"