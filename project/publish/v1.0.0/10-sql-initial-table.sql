CREATE DATABASE IF NOT EXISTS `flower` DEFAULT CHARACTER SET utf8mb4;
USE `flower`;

CREATE TABLE IF NOT EXISTS `account` (
  `id` bigint(20) NOT NULL,
  `name` varchar(36) NOT NULL COMMENT '用户名',
  `secret` varchar(256) NOT NULL COMMENT '密钥',
  `salt` varchar(256) NOT NULL COMMENT '加盐',
  `status` tinyint(1) unsigned NOT NULL COMMENT '状态',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `expired_time` datetime NOT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='账号';


CREATE TABLE IF NOT EXISTS `author` (
  `id` bigint(20) NOT NULL,
  `name` varchar(36) NOT NULL COMMENT '名称',
  `avatar` varchar(256) NOT NULL COMMENT '头像',
  `status` varchar(36) NOT NULL DEFAULT '' COMMENT '状态',
  `gender` varchar(36) NOT NULL DEFAULT '' COMMENT '性别',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='作者';

CREATE TABLE IF NOT EXISTS `global_activation` (
  `id` bigint(20) NOT NULL,
  `user_id` varchar(36) NOT NULL COMMENT '用户码',
  `auth_id` varchar(36) NOT NULL COMMENT '认证码',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `expired_time` datetime NOT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='验证码';

CREATE TABLE IF NOT EXISTS `global_district` (
  `id` bigint(20) NOT NULL,
  `name` varchar(36) NOT NULL COMMENT '名称',
  `title` varchar(128) NOT NULL COMMENT '全称',
  `type` tinyint(1) unsigned NOT NULL COMMENT '类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='行政区域';
