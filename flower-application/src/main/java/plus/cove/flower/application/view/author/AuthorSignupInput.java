package plus.cove.flower.application.view.author;

import lombok.Data;
import plus.cove.flower.domain.entity.account.Account;
import plus.cove.flower.domain.entity.account.AccountBuilder;
import plus.cove.flower.domain.entity.author.Author;

import javax.validation.constraints.NotEmpty;

/**
 * 作者
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class AuthorSignupInput {
    @NotEmpty(message = "邮箱不能为空")
    private String email;
    @NotEmpty(message = "密码不能为空")
    private String password;

    @NotEmpty(message = "用户名不能为空")
    private String nickName;

    public Account toAccount(Long userId) {
        return AccountBuilder.init()
                .user(userId)
                .name(this.email)
                .password(this.password)
                .build();
    }

    public Author toAuthor() {
        return Author.create(this.nickName);
    }
}
