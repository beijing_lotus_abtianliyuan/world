package plus.cove.flower.application.view.author;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import plus.cove.flower.domain.entity.author.Author;

@Mapper(componentModel = "cdi")
public interface AuthorMineMapper {
    AuthorMineMapper INSTANCE = Mappers.getMapper(AuthorMineMapper.class);

    AuthorMineOutput convertTo(Author input);
}
