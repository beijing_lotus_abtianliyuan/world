package plus.cove.flower.application;

import plus.cove.flower.application.view.district.DistrictQueryInput;
import plus.cove.flower.application.view.district.DistrictQueryOutput;
import plus.cove.flower.domain.entity.global.District;

import java.util.List;

/**
 * 城市应用
 * <p>
 * 中华人民共和国民政部-行政区划代码
 * http://www.mca.gov.cn/article/sj/xzqh/2020/
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public interface DistrictApplication {
    /**
     * 获取城市列表
     *
     * @since 1.0
     */
    List<DistrictQueryOutput> loadDistrict(DistrictQueryInput input);

    /**
     * 保存城市
     *
     * @since 4.0
     */
    void saveDistrict(District entity);
}
