package plus.cove.flower.application.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import plus.cove.flower.application.DistrictApplication;
import plus.cove.flower.application.view.district.DistrictQueryInput;
import plus.cove.flower.application.view.district.DistrictQueryMapper;
import plus.cove.flower.application.view.district.DistrictQueryOutput;
import plus.cove.flower.domain.entity.global.District;
import plus.cove.flower.domain.repository.DistrictRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * 地区应用
 * <p>
 * 中华人民共和国民政部-行政区划代码
 * http://www.mca.gov.cn/article/sj/xzqh/2020/
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class DistrictApplicationImpl implements DistrictApplication {
    private final Logger log = LoggerFactory.getLogger(DistrictApplicationImpl.class);
    private final DistrictRepository districtRep;

    public DistrictApplicationImpl(final DistrictRepository districtRep) {
        this.districtRep = districtRep;
    }

    @Override
    public List<DistrictQueryOutput> loadDistrict(DistrictQueryInput input) {
        // 获取列表
        District entity = DistrictQueryMapper.INSTANCE.convertOf(input);
        List<District> list = districtRep.selectEntity(entity);

        // 组装数据
        List<DistrictQueryOutput> districts = new ArrayList<>();
        int maxLen = list.size();
        for (int i = 0; i < maxLen; i++) {
            // 查找第一级
            District one = list.get(i);
            DistrictQueryOutput district = DistrictQueryOutput.from(one);

            // 查找第二级
            for (int j = i + 1; j < maxLen; j++) {
                District two = list.get(j);
                if (two.getType() == one.getType()) {
                    break;
                }
                district.addDistrict(two);
                i++;
            }

            // 组装
            districts.add(district);
        }

        return districts;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void saveDistrict(District entity) {
        this.districtRep.insertEntity(entity);
    }
}
