package plus.cove.flower.application.view.district;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import plus.cove.flower.domain.entity.global.District;

@Mapper(componentModel = "cdi")
public interface DistrictQueryMapper {
    DistrictQueryMapper INSTANCE = Mappers.getMapper(DistrictQueryMapper.class);

    District convertOf(DistrictQueryInput input);
}
