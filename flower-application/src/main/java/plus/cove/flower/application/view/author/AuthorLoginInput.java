package plus.cove.flower.application.view.author;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 作者
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class AuthorLoginInput {
    @NotEmpty(message = "用户名不能为空")
    private String userName;

    @NotEmpty(message = "密码不能为空")
    private String password;
}
