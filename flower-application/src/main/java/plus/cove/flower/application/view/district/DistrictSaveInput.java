package plus.cove.flower.application.view.district;

import lombok.Data;
import plus.cove.flower.domain.entity.global.District;
import plus.cove.flower.domain.entity.global.DistrictType;

import javax.enterprise.context.ApplicationScoped;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 地区
 *
 * @author jimmy.zhang
 * @since 4.0
 */
@Data
@ApplicationScoped
public class DistrictSaveInput {
    /**
     * 类型
     */
    @NotNull(message = "类型不能为空")
    private DistrictType type;

    @NotBlank(message = "名称不能为空")
    private String name;

    public District toEntity() {
        District entity = new District();
        entity.setType(this.type);
        entity.setName(this.name);
        return entity;
    }
}
