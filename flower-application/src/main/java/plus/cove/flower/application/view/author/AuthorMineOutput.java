package plus.cove.flower.application.view.author;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

/**
 * 作者
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class AuthorMineOutput {
    @JsonIgnore
    private Long id;
    private String name;
    private String phone;
    private String avatar;
    private String status;

    /**
     * 照片数量
     */
    private Integer photoCount;
}
