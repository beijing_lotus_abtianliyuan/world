package plus.cove.flower.application.view.author;

import lombok.Data;

/**
 * 作者
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class AuthorSignupOutput {
    private String token;

    public static AuthorSignupOutput from(String token) {
        AuthorSignupOutput output = new AuthorSignupOutput();
        output.token = token;
        return output;
    }
}
