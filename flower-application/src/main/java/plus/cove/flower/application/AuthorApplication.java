package plus.cove.flower.application;

import plus.cove.flower.application.view.author.*;

/**
 * 作者应用
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public interface AuthorApplication {
    /**
     * 注册
     * <p>
     * 创建作者
     */
    AuthorSignupOutput signupAuthor(AuthorSignupInput entity);

    /**
     * 登录
     */
    AuthorLoginOutput loginAuthor(AuthorLoginInput input);

    /**
     * 获取作者
     */
    AuthorMineOutput loadAuthor(Long id);
}
