package plus.cove.flower.application.view.district;

import lombok.Data;
import plus.cove.flower.domain.entity.global.District;

import java.util.ArrayList;
import java.util.List;

@Data
public class DistrictQueryOutput {
    /**
     * 编号
     */
    private Long id;

    /**
     * 名称
     */
    private String name;

    /**
     * 下级区域
     */
    private List<DistrictQueryOutput> districts;

    public void addDistrict(District entity) {
        if (this.districts == null) {
            this.districts = new ArrayList<>();
        }
        DistrictQueryOutput output = DistrictQueryOutput.from(entity);
        this.districts.add(output);
    }

    public static DistrictQueryOutput from(District entity) {
        DistrictQueryOutput output = new DistrictQueryOutput();
        output.id = entity.getId();
        output.name = entity.getName();
        return output;
    }
}
