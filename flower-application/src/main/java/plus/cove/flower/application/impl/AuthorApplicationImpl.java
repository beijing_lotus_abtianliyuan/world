package plus.cove.flower.application.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import plus.cove.flower.application.AuthorApplication;
import plus.cove.flower.application.view.author.*;
import plus.cove.flower.domain.entity.account.Account;
import plus.cove.flower.domain.entity.account.AccountError;
import plus.cove.flower.domain.entity.author.Author;
import plus.cove.flower.domain.repository.AccountRepository;
import plus.cove.flower.domain.repository.AuthorRepository;
import plus.cove.infrastructure.extension.authorizer.JwtClaimUtils;
import plus.cove.infrastructure.extension.authorizer.JwtResult;
import plus.cove.infrastructure.helper.AssertHelper;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

/**
 * 作者应用
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class AuthorApplicationImpl implements AuthorApplication {
    private final Logger log = LoggerFactory.getLogger(AuthorApplicationImpl.class);
    private final AccountRepository accountRep;
    private final AuthorRepository authorRep;

    private JwtClaimUtils jwtUtil;

    public AuthorApplicationImpl(
            final AccountRepository accountRep,
            final AuthorRepository authorRep,
            JwtClaimUtils jwtUtil
    ) {
        this.accountRep = accountRep;
        this.authorRep = authorRep;
        this.jwtUtil = jwtUtil;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public AuthorSignupOutput signupAuthor(AuthorSignupInput input) {
        // 查找account是否存在，存在则报错
        Account account = accountRep.selectByName(input.getEmail());
        AssertHelper.assertNull(account, AccountError.EXISTED_ACCOUNT);

        // 生成实体并保存
        Author author = input.toAuthor();
        account = input.toAccount(author.getId());

        authorRep.insertEntity(author);
        accountRep.insertEntity(account);

        // 生成token
        JwtResult token = jwtUtil.build(account.getUserId().toString());
        AuthorSignupOutput output = AuthorSignupOutput.from(token.getToken());
        return output;
    }

    @Override
    public AuthorLoginOutput loginAuthor(AuthorLoginInput input) {
        // 根据用户名获取
        Account account = accountRep.selectByName(input.getUserName());
        AssertHelper.assertNotNull(account, AccountError.INVALID_USER_NAME);

        // 验证密码
        boolean verify = account.verifyPassword(input.getPassword());
        AssertHelper.assertTrue(verify, AccountError.INVALID_PASSWORD);

        // 验证状态
        verify = account.verifyState();
        AssertHelper.assertTrue(verify, AccountError.INVALID_STATUS);

        // 生成Token
        JwtResult token = jwtUtil.build(account.getUserId().toString());
        return AuthorLoginOutput.from(token.getToken());
    }

    @Override
    public AuthorMineOutput loadAuthor(Long id) {
        Author entity = this.authorRep.selectById(id);
        return AuthorMineMapper.INSTANCE.convertTo(entity);
    }
}
