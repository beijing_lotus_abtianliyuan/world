package plus.cove.flower.application.view.author;

import lombok.Data;

/**
 * 作者
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class AuthorLoginOutput {
    private String token;

    public static AuthorLoginOutput from(String token) {
        AuthorLoginOutput output = new AuthorLoginOutput();
        output.token = token;
        return output;
    }
}
