package plus.cove.flower.application.view.district;

import lombok.Data;

import javax.enterprise.context.RequestScoped;
import javax.validation.constraints.NotBlank;


/**
 * 地区
 *
 * @author jimmy.zhang
 * @since 4.0
 */
@Data
@RequestScoped
public class DistrictQueryInput {
    @NotBlank(message = "名称不能为空")
    private String name;

    public static DistrictQueryInput from(String name) {
        DistrictQueryInput input = new DistrictQueryInput();
        input.name = name;
        return input;
    }
}
