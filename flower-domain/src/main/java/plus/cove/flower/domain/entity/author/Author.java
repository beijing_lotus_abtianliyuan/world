package plus.cove.flower.domain.entity.author;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import plus.cove.infrastructure.component.BaseTimeEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 用户信息
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@Table(name = "author")
@EqualsAndHashCode(callSuper = true)
public class Author extends BaseTimeEntity {
    private String name;
    private String phone;
    private String avatar;
    private String status;

    /**
     * 创建
     *
     * @param
     * @return
     */
    public static Author create(String name) {
        Author author = new Author();
        author.valueOf();
        author.name = name;
        author.phone = StrUtil.EMPTY;
        author.status = StrUtil.EMPTY;
        author.avatar = StrUtil.EMPTY;
        return author;
    }
}
