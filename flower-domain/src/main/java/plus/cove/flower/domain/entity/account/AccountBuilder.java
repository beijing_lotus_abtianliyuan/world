package plus.cove.flower.domain.entity.account;

import cn.hutool.core.util.RandomUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 账号建造器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public final class AccountBuilder {
    private final Logger log = LoggerFactory.getLogger(AccountBuilder.class);
    private Account entity = null;

    public static AccountBuilder init() {
        AccountBuilder builder = new AccountBuilder();
        builder.entity = Account.create();
        return builder;
    }

    public AccountBuilder name(String name) {
        this.entity.setName(name);
        return this;
    }

    public AccountBuilder user(Long userId) {
        this.entity.setUserId(userId);
        return this;
    }

    public AccountBuilder password(String password) {
        // 生成salt
        String salt = RandomUtil.randomString(32);
        this.entity.setSalt(salt);

        // 生成secret
        String secret = entity.buildPassword(password);
        this.entity.setSecret(secret);

        return this;
    }

    public Account build() {
        return this.entity;
    }
}
