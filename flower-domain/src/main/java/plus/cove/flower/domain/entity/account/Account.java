package plus.cove.flower.domain.entity.account;

import cn.hutool.crypto.digest.DigestUtil;
import lombok.Data;
import plus.cove.infrastructure.component.BaseEntity;
import plus.cove.infrastructure.helper.AssertHelper;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * 账号实体类
 * <p>
 * 包括账号信息，密码等
 * 与业务没有关系
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@Table(name = "account")
public class Account extends BaseEntity {
    /**
     * 名称
     */
    private String name;

    /**
     * 状态
     */
    private AccountState state;

    /**
     * 加盐
     */
    private String salt;

    /**
     * 密码
     * 加密后存储
     */
    private String secret;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 生成密码
     */
    public String buildPassword(String password) {
        AssertHelper.assertNotEmpty(password, "password is empty");

        String secret = DigestUtil.md5Hex(password + this.salt);
        return secret;
    }

    /**
     * 验证密码
     */
    public boolean verifyPassword(String password) {
        String secret = this.buildPassword(password);
        return secret.equals(this.secret);
    }

    /**
     * 验证状态
     *
     * @return
     */
    public boolean verifyState() {
        return this.state == AccountState.ACTIVE;
    }

    /**
     * 创建
     * <p>
     * 包括id
     */
    public static Account create() {
        Account entity = new Account();
        entity.valueOf();
        entity.state = AccountState.ACTIVE;
        entity.createTime = LocalDateTime.now();
        return entity;
    }
}
