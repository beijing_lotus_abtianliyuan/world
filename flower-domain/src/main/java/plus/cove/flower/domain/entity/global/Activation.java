package plus.cove.flower.domain.entity.global;

import lombok.Data;
import lombok.EqualsAndHashCode;
import plus.cove.infrastructure.component.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * 激活码
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@Table(name = "global_activation")
@EqualsAndHashCode(callSuper = true)
public class Activation extends BaseEntity {
    private String userCode;
    private String authCode;
    private LocalDateTime createTime;
    private LocalDateTime expiredTime;

    /**
     * 创建用户激活实体
     *
     * @param
     * @return
     */
    public static Activation create(String userCode, String authCode, long expMinutes) {
        Activation entity = new Activation();
        entity.valueOf();

        LocalDateTime createTime = LocalDateTime.now();
        LocalDateTime expiredTime = createTime.plusMinutes(expMinutes);
        entity.createTime = createTime;
        entity.expiredTime = expiredTime;
        entity.userCode = userCode;
        entity.authCode = authCode;
        return entity;
    }

    /**
     * 是否有效
     *
     * @param
     * @return
     */
    public boolean isValid() {
        return this.expiredTime.isAfter(LocalDateTime.now());
    }
}
