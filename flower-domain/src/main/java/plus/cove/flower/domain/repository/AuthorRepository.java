package plus.cove.flower.domain.repository;

import plus.cove.flower.domain.entity.author.Author;

import javax.enterprise.context.ApplicationScoped;

/**
 * 作者仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public interface AuthorRepository {
    /**
     * 获取作者
     *
     * @since 4.7
     */
    Author selectById(Long id);

    /**
     * 插入作者
     *
     * @param entity
     */
    void insertEntity(Author entity);
}
