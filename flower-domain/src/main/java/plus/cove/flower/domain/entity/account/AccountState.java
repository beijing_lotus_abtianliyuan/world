package plus.cove.flower.domain.entity.account;

import plus.cove.infrastructure.component.BaseEnum;

/**
 * 账号状态
 *
 * @param
 * @author jimmy.zhang
 * @since 1.0
 */
public enum AccountState implements BaseEnum {
    NONE(0, "未知的"),
    ACTIVE(1, "激活的"),
    DISABLED(2, "禁用的");

    /**
     * 枚举值
     */
    private final int value;

    /**
     * 枚举描述
     */
    private final String desc;

    /**
     * 构造函数
     */
    AccountState(final int value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDesc() {
        return desc;
    }
}
