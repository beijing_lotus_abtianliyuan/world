package plus.cove.flower.domain.repository;

import plus.cove.flower.domain.entity.global.District;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

/**
 * 账号仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public interface DistrictRepository {
    /**
     * 获取列表
     *
     * @since 1.0
     */
    List<District> selectEntity(District entity);

    /**
     * 插入实体
     *
     * @since 1.0
     */
    void insertEntity(District entity);
}

