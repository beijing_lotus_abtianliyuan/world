package plus.cove.flower.domain.repository;

import plus.cove.flower.domain.entity.account.Account;

import javax.enterprise.context.ApplicationScoped;

/**
 * 账号仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public interface AccountRepository {
    /**
     * 获取账号
     *
     * @param name 用户名
     * @return 用户名对应的账号，没有记录返回 null
     */
    Account selectByName(String name);

    /**
     * 插入账号
     *
     * @param entity
     */
    void insertEntity(Account entity);
}
