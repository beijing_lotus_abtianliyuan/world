package plus.cove.flower.domain.entity.account;

import plus.cove.infrastructure.exception.BusinessError;

/**
 * 账号错误
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum AccountError implements BusinessError {
    EXISTED_ACCOUNT("A1000", "该账号已存在"),
    INVALID_USER_NAME("A1100", "无效的账号"),
    INVALID_PASSWORD("A1200", "无效的密码"),
    INVALID_STATUS("A1300", "无效的状态或已过期"),

    INVALID_ACTIVATION("A2000", "无效的激活码"),
    EXPIRED_ACTIVATION("A2100", "激活码已过期"),
    USED_ACTIVATION("A2200", "账号已激活"),

    INVALID_LIMIT("A3000", "超过登陆错误上限"),
    INVALID_VERSION("A3100", "无效的随机码");

    /**
     * 枚举值
     */
    private final String code;

    /**
     * 枚举描述
     */
    private String message;

    /**
     * 构造函数
     *
     * @param
     * @return
     */
    AccountError(final String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}
