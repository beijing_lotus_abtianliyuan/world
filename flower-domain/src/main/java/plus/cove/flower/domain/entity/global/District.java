package plus.cove.flower.domain.entity.global;

import lombok.Data;
import lombok.EqualsAndHashCode;
import plus.cove.infrastructure.component.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 城市
 * 包括城市名称，城市中心坐标
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@Table(name = "global_district")
@EqualsAndHashCode(callSuper = true)
public class District extends BaseEntity {
    /**
     * 名称
     */
    private String name;

    /**
     * 全称
     */
    private String title;

    /**
     * 类型
     */
    private DistrictType type;
}
