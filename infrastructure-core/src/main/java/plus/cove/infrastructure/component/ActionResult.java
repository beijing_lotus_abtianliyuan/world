package plus.cove.infrastructure.component;

import lombok.Getter;
import lombok.ToString;
import plus.cove.infrastructure.exception.BusinessError;
import plus.cove.infrastructure.exception.BusinessException;

/**
 * 操作结果
 * <p>
 * 业务的返回，包括
 * 1 非正常输入
 * 2 业务逻辑错误
 * 3 其他系统捕获错误
 * <p>
 * 针对调用成功返回，但结果未知的情况，code=0，业务数据各自返回
 * <p>
 * 前台获取后，根据code进行判断是否成功
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ToString
public class ActionResult<T> {
    @Getter
    private String code;

    @Getter
    private String message;

    @Getter
    private T data;

    /**
     * 默认构造函数
     */
    private ActionResult() {
    }

    /**
     * 全参构造函数
     *
     * @param code
     * @param message
     */
    private ActionResult(String code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 默认的静态函数
     *
     * @param
     * @return
     */
    public static ActionResult result() {
        return new ActionResult();
    }

    /**
     * 成功的静态方法
     *
     * @param
     * @return
     */
    public static ActionResult success() {
        return new ActionResult("0", "ok");
    }

    /**
     * 失败的静态方法
     *
     * @param
     * @return
     */
    public static ActionResult failure(BusinessError error) {
        return new ActionResult(error.code(), error.message());
    }

    /**
     * 成功
     *
     * @param data 成功返回数据
     */
    public ActionResult<T> succeed(T data) {
        this.code = "0";
        this.message = "ok";
        this.data = data;
        return this;
    }

    /**
     * 失败
     *
     * @param error 错误枚举
     */
    public ActionResult fail(BusinessError error) {
        this.code = error.code();
        this.message = error.message();
        return this;
    }

    /**
     * 失败
     *
     * @param
     * @return
     */
    public ActionResult fail(BusinessException except) {
        this.code = except.getCode();
        this.message = except.getMessage();
        return this;
    }

    /**
     * 失败
     *
     * @param error 错误枚举
     * @param data  错误数据
     */
    public ActionResult<T> fail(BusinessError error, T data) {
        this.fail(error);
        this.data = data;
        return this;
    }

    /**
     * 消息
     *
     * @param
     * @return
     */
    public ActionResult message(String message) {
        this.message = message;
        return this;
    }

    /**
     * 输出简单json格式
     *
     * @param
     * @return
     * @author jimmy.zhang
     * @since 3.6
     */
    public String toJson() {
        StringBuilder json = new StringBuilder();
        json.append("{");
        json.append("\"code\":");
        json.append(code);
        json.append(",\"message\":");

        if (message == null) {
            json.append("null");
        } else {
            json.append("\"");
            json.append(message);
            json.append("\"");
        }
        json.append("}");
        return json.toString();
    }
}
