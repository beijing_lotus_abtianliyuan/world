package plus.cove.infrastructure.generator;

/**
 * 主键生成器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public interface KeyGenerator {
    /**
     * 获取类型
     *
     * @param
     * @return
     */
    String getType();

    /**
     * 生成键
     *
     * @param
     * @return
     */
    long generateKey();
}
