package plus.cove.infrastructure.exception;

/**
 * 服务错误
 * 20-29
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum ServiceError implements BusinessError {
    // 无效请求，请求用户错误，比如：认证失败，过期，前台返回登陆
    BAD_REQUEST("401", "无效请求"),
    // 无效权限，请求用户没有权限，比如：该资源无操作权限，前台提示错误
    BAD_PERMIT("402", "无效权限"),
    // 无效页面
    NOT_FOUND("404", "无效页面"),
    // 无效版本，请求版本和接口版本不一致，比如：发布新版，前台未刷新，前台强制刷新
    NOT_ALLOWED("405", "无效版本"),

    // 其他未知错误
    SERVER_ERROR("500", "内部错误");

    /**
     * 枚举值
     */
    private final String code;

    /**
     * 枚举描述
     */
    private final String message;

    /**
     * 构造函数
     *
     */
    ServiceError(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}

