package plus.cove.infrastructure.exception;

/**
 * 文件异常
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum SystemError implements BusinessError {
    IMPORT_DATA_EMPTY("200","上传数据为空"),
    IMPORT_DATA_ROW("201", "上传数据超过最大限制，请确认是否超过最大行数"),
    IMPORT_INVALID_TEMPLATE("202", "请使用在本系统下载的模板进行上传，否则无法识别"),

    FILE_NOT_FOUND("210", "文件未找到"),
    FILE_CREATE_ERROR("211", "创建失败"),
    FILE_READ_ERROR("212", "读取失败"),
    FILE_WRITE_ERROR("213", "写失败");

    /**
     * 枚举值
     */
    private final String code;

    /**
     * 枚举描述
     */
    private final String message;

    /**
     * 构造函数
     *
     */
    SystemError(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}