package plus.cove.infrastructure.exception;

/**
 * 业务异常
 * <p>
 * 所有业务异常的基类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class BusinessException extends RuntimeException {
    /**
     * 编码，唯一编码
     */
    private final String code;

    /**
     * 信息，错误信息
     */
    private String message;

    public BusinessException(BusinessError error) {
        super(error.message(), null, false, false);
        this.code = error.code();
        this.message = error.message();
    }

    public BusinessException(BusinessError error, Throwable cause) {
        super(error.message(), cause, false, false);
        this.code = error.code();
        this.message = error.message();
    }

    public BusinessException(BusinessError error, String message) {
        super(message, null, false, false);
        this.code = error.code();
        this.message = message;
    }

    public String getCode(){
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public BusinessException message(String message){
        this.message = message;
        return this;
    }

    @Override
    public String toString() {
        return String.format("%s: code=%d, message=%s",
                this.getClass().getName(),
                this.code,
                this.message == null ? "null" : this.message);
    }

    /**
     * 从错误获取异常信息
     *
     * @param error 异常枚举
     * @return
     */
    public static BusinessException from(BusinessError error) {
        return new BusinessException(error);
    }
}
