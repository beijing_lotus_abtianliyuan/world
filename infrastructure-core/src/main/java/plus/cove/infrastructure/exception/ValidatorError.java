package plus.cove.infrastructure.exception;

/**
 * 验证错误
 * code 范围：10-19
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum ValidatorError implements BusinessError {
    INVALID_CONSTRAINT("100", "无效的验证规则"),
    INVALID_ARGUMENT("101", "无效的参数"),

    JWT_CREATION_EXCEPTION("110", "生成TOKEN异常"),
    JWT_DECODE_EXCEPTION("111", "解码TOKEN异常"),
    JWT_VERIFICATION_EXCEPTION("112", "验证TOKEN异常"),
    JWT_INVALID_SECRET("113", "无效的TOKEN秘钥"),

    CONVERTER_INVALID_ORIGIN("120", "无效的来源数据"),
    CONVERTER_INVALID_TARGET("121", "无效的目标数据"),
    CONVERTER_INVALID_TYPE("122", "无效的类型类别"),

    IDEMPOTENT_TOKEN_MISSING("130", "无效的提交参数"),
    IDEMPOTENT_TOKEN_DUPLICATED("131", "重复提交数据"),

    RETRY_FAILURE("150", "重试失败"),
    VERSION_FAILURE("160", "获取版本失败");

    /**
     * 枚举值
     */
    private final String code;

    /**
     * 枚举描述
     */
    private final String message;

    ValidatorError(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return this.code;
    }

    @Override
    public String message() {
        return this.message;
    }
}

