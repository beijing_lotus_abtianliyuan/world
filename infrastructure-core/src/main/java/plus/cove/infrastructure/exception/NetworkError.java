package plus.cove.infrastructure.exception;

/**
 * 网络异常
 * 包括邮件
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum NetworkError implements BusinessError {
    MOVED_PERMANENTLY("301", "Moved Permanently"),
    HAD_FOUND("302","Found"),
    NOT_MODIFIED("304", "Not Modified"),

    MAIL_SUBJECT_ERROR("310", "无效的主题"),
    MAIL_FROM_ERROR("311", "无效的发件人"),
    MAIL_TO_ERROR("312", "无效的收件人"),
    MAIL_CONTENT_ERROR("313", "邮件内容无效"),
    MAIL_SEND_ERROR("314", "邮件发送失败"),

    REST_GET_ERROR("320", "HTTP GET请求错误"),
    REST_POST_ERROR("321", "HTTP POST请求错误"),
    REST_PUT_ERROR("322", "HTTP PUT请求错误"),
    REST_DELETE_ERROR("323", "HTTP DELETE请求错误");

    /**
     * 枚举值
     */
    private final String code;

    /**
     * 枚举描述
     */
    private final String message;

    /**
     * 构造函数
     *
     */
    NetworkError(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}