package plus.cove.infrastructure.helper;

/**
 * base64工具类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class Base64Helper {
    // base64格式
    private static final String PATTERN_BASE64 = "base64,";

    /**
     * 私有构造器
     */
    private Base64Helper() {
    }

    public static String trim(String base64) {
        if (base64 == null || base64.equals("") || base64.length() < PATTERN_BASE64.length()) {
            return base64;
        }

        int index = base64.indexOf(PATTERN_BASE64) + PATTERN_BASE64.length();
        int length = base64.length();

        return base64.substring(index, length);
    }
}
