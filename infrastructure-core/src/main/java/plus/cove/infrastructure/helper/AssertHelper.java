package plus.cove.infrastructure.helper;

import plus.cove.infrastructure.exception.BusinessError;
import plus.cove.infrastructure.exception.BusinessException;
import plus.cove.infrastructure.exception.ValidatorError;

import java.util.Collection;
import java.util.Objects;

/**
 * 预校验工具
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class AssertHelper {
    private AssertHelper() {
    }

    /**
     * 真断言
     * <p>
     * 需要是真
     *
     * @param
     * @return
     */
    public static void assertTrue(boolean expression, String message) {
        if (!expression) {
            throw new BusinessException(ValidatorError.INVALID_ARGUMENT).message(message);
        }
    }

    /**
     * 真断言
     * <p>
     * 需要是真
     *
     * @param
     * @return
     */
    public static void assertTrue(boolean expression, BusinessError error) {
        if (!expression) {
            throw BusinessException.from(error);
        }
    }

    /**
     * 假断言
     * <p>
     * 需要是假
     */
    public static void assertFalse(boolean expression, String message) {
        if (expression) {
            throw new BusinessException(ValidatorError.INVALID_ARGUMENT).message(message);
        }
    }

    /**
     * 假断言
     * <p>
     * 需要是假
     */
    public static void assertFalse(boolean expression, BusinessError error) {
        if (expression) {
            throw BusinessException.from(error);
        }
    }

    /**
     * object为null
     */
    public static void assertNull(Object object, String message) {
        if (!Objects.isNull(object)) {
            throw new BusinessException(ValidatorError.INVALID_ARGUMENT).message(message);
        }
    }

    /**
     * object为null
     */
    public static void assertNull(Object object, BusinessError error) {
        if (!Objects.isNull(object)) {
            throw BusinessException.from(error);
        }
    }

    /**
     * object不为null
     *
     * @param
     * @return
     */
    public static void assertNotNull(Object object, String message) {
        if (Objects.isNull(object)) {
            throw new BusinessException(ValidatorError.INVALID_ARGUMENT).message(message);
        }
    }

    /**
     * object不为null
     *
     * @param
     * @return
     */
    public static void assertNotNull(Object object, BusinessError error) {
        if (Objects.isNull(object)) {
            throw BusinessException.from(error);
        }
    }

    /**
     * string为空
     *
     * @param
     * @return
     */
    public static void assertEmpty(String object, String message) {
        if (StringHelper.isNotEmpty(object)) {
            throw new BusinessException(ValidatorError.INVALID_ARGUMENT).message(message);
        }
    }

    /**
     * object为空
     *
     * @param
     * @return
     */
    public static void assertEmpty(String object, BusinessError error) {
        if (StringHelper.isNotEmpty(object)) {
            throw BusinessException.from(error);
        }
    }

    /**
     * string不为空
     *
     * @param
     * @return
     */
    public static void assertNotEmpty(String object, String message) {
        if (StringHelper.isEmpty(object)) {
            throw new BusinessException(ValidatorError.INVALID_ARGUMENT).message(message);
        }
    }

    /**
     * string不为空
     *
     * @param
     * @return
     */
    public static void assertNotEmpty(String object, BusinessError error) {
        if (StringHelper.isEmpty(object)) {
            throw BusinessException.from(error);
        }
    }

    /**
     * list不为空
     *
     * @param
     * @return
     */
    public static void assertList(Collection object, String message) {
        if (CollectionHelper.isEmpty(object)) {
            throw new BusinessException(ValidatorError.INVALID_ARGUMENT).message(message);
        }
    }

    /**
     * list不为空
     *
     * @param
     * @return
     */
    public static void assertList(Collection object, BusinessError error) {
        if (CollectionHelper.isEmpty(object)) {
            throw BusinessException.from(error);
        }
    }

    /**
     * list为空
     *
     * @param
     * @return
     */
    public static void assertEmptyList(Collection object, String message) {
        if (CollectionHelper.isNotEmpty(object)) {
            throw new BusinessException(ValidatorError.INVALID_ARGUMENT).message(message);
        }
    }

    /**
     * list为空
     *
     * @param
     * @return
     */
    public static void assertEmptyList(Collection object, BusinessError error) {
        if (CollectionHelper.isNotEmpty(object)) {
            throw BusinessException.from(error);
        }
    }
}
