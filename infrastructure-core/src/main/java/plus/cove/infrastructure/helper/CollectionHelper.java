package plus.cove.infrastructure.helper;

import java.util.Collection;
import java.util.function.BiConsumer;

/**
 * 集合帮助类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class CollectionHelper {
    private CollectionHelper() {
    }

    /**
     * 是否空
     */
    public static boolean isEmpty(Collection source) {
        return source == null || source.isEmpty();
    }

    /**
     * 不为空
     */
    public static boolean isNotEmpty(Collection source) {
        return !isEmpty(source);
    }

    /**
     * 合并
     *
     */
    public static <T, E> void combine(Collection<T> origin, Collection<E> target, BiConsumer<T, E> consumer) {
        if (CollectionHelper.isEmpty(origin) || CollectionHelper.isEmpty(target)) {
            return;
        }

        // 双循环
        for (T ori : origin) {
            for (E tar : target) {
                consumer.accept(ori, tar);
            }
        }
    }
}
