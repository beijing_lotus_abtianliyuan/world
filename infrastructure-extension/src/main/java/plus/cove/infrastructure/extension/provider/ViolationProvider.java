package plus.cove.infrastructure.extension.provider;

import plus.cove.infrastructure.component.ActionResult;
import plus.cove.infrastructure.exception.ValidatorError;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Bean Validation
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Provider
public class ViolationProvider implements ExceptionMapper<ConstraintViolationException> {
    @Override
    public Response toResponse(ConstraintViolationException exception) {
        StringBuilder sbError = new StringBuilder();
        for (ConstraintViolation<?> violation : exception.getConstraintViolations()) {
            sbError.append(violation.getPropertyPath().toString()).append(":");
            sbError.append(violation.getMessage()).append(",");
        }

        ActionResult result = ActionResult.result();
        result.fail(ValidatorError.INVALID_ARGUMENT, sbError.toString());

        return Response.status(Response.Status.BAD_REQUEST)
                .entity(result)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
