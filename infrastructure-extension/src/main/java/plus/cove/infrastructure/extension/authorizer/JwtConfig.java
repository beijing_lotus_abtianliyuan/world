package plus.cove.infrastructure.extension.authorizer;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * jwt配置
 * 设置jwt相关参数
 *
 * @author jimmy.zhang
 * @ 2019-04-28
 */
@ConfigMapping(prefix = "summer.jwt-config")
public interface JwtConfig {
    /**
     * Token秘钥
     *
     * @return 密钥
     */
    String secret();

    /**
     * Token过期时间
     * 单位秒，默认72小时
     *
     * @return 过期时间
     */
    Long expired();

    /**
     * Token主体
     *
     * @return 主体
     */
    @WithDefault("sub")
    String subject();

    /**
     * Token发行人
     *
     * @return 发行人
     */
    @WithDefault("iss")
    String issuer();

    /**
     * Token身份
     *
     * @return 身份
     */
    @WithDefault("id")
    String claim();

    /**
     * TOKEN角色
     *
     * @return 角色
     */
    @WithDefault("user")
    String actor();

    /**
     * Token其他
     *
     * @return 其他
     */
    @WithDefault("extra")
    String extra();

    /**
     * Token 认证来源
     * header及query
     *
     * @return 认证来源
     */
    @WithDefault("Authorization")
    String tokenSource();

    /**
     * Token 认证标识
     *
     * @return 认证标识
     */
    @WithDefault("Bearer")
    String tokenBearer();
}
