package plus.cove.infrastructure.extension.provider;

import plus.cove.infrastructure.component.ActionResult;
import plus.cove.infrastructure.exception.BusinessException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Bean Validation
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Provider
public class ValidationProvider implements ExceptionMapper<BusinessException> {
    @Override
    public Response toResponse(BusinessException exception) {
        ActionResult result = ActionResult.result();
        result.fail(exception);

        return Response.status(Response.Status.BAD_REQUEST)
                .entity(result)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
