package plus.cove.infrastructure.extension.authorizer;

import lombok.Getter;

import java.util.Set;

/**
 * jwt输出结果
 * 包括是否成功和身份
 *
 * @author jimmy.zhang
 * @date 2019-05-15
 */
public class JwtClaim {
    /**
     * 声明
     */
    @Getter
    private String claim;

    @Getter
    private String extra;

    @Getter
    private Set<String> actors;

    public static JwtClaim of(String claim, String extra, Set<String> actors) {
        JwtClaim jwt = new JwtClaim();
        jwt.claim = claim;
        jwt.extra = extra;
        jwt.actors = actors;
        return jwt;
    }
}
