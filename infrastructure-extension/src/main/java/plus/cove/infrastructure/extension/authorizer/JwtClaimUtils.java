package plus.cove.infrastructure.extension.authorizer;

import io.smallrye.jwt.auth.principal.JWTParser;
import io.smallrye.jwt.auth.principal.ParseException;
import io.smallrye.jwt.build.Jwt;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.enterprise.context.ApplicationScoped;
import java.util.Set;

/**
 * jwt工具类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class JwtClaimUtils {
    private final JwtConfig jwtConfig;
    private final JWTParser jwtParser;

    public JwtClaimUtils(
            final JwtConfig jwtConfig,
            final JWTParser jwtParser) {
        this.jwtConfig = jwtConfig;
        this.jwtParser = jwtParser;
    }

    /**
     * 创建token
     *
     * @since 1.0
     */
    public JwtResult build(String claim, String extra, String... group) {
        String token = Jwt.upn(claim)
                .issuer(jwtConfig.issuer())
                .subject(jwtConfig.subject())
                .expiresIn(jwtConfig.expired())
                .claim(jwtConfig.claim(), claim)
                .claim(jwtConfig.extra(), extra)
                .groups(Set.of(group))
                .signWithSecret(jwtConfig.secret());

        JwtResult result = JwtResult.of(token, jwtConfig.expired());
        return result;
    }

    /**
     * 创建token
     *
     * @since 1.0
     */
    public JwtResult build(String claim) {
        return build(claim, "extra", jwtConfig.actor());
    }

    /**
     * 解密
     *
     * @since 1.0
     */
    public JwtClaim parse(String token) throws ParseException {
        JsonWebToken wt = jwtParser.verify(token, jwtConfig.secret());
        return JwtClaim.of(
                wt.getClaim(jwtConfig.claim()),
                wt.getClaim(jwtConfig.extra()),
                wt.getGroups());
    }
}
