package plus.cove.infrastructure.extension.authorizer;

import lombok.Getter;

/**
 * jwt输入结果
 * 包括token和过期时间
 *
 * @author jimmy.zhang
 * @date 2019-05-15
 */
public class JwtResult {
    /**
     * token值
     */
    @Getter
    private String token;

    /**
     * 过期时间
     * 单位秒
     */
    @Getter
    private Long expire;

    private JwtResult() {
    }

    public static JwtResult of(String token, Long expire) {
        JwtResult result = new JwtResult();
        result.expire = expire;
        result.token = token;
        return result;
    }
}
