package plus.cove.flower.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import plus.cove.flower.domain.entity.author.Author;
import plus.cove.flower.domain.repository.AuthorRepository;
import plus.cove.flower.repository.mapper.AuthorMapper;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class AuthorRepositoryImpl implements AuthorRepository {
    private final Logger log = LoggerFactory.getLogger(AuthorRepositoryImpl.class);
    private AuthorMapper authorMapper;

    public AuthorRepositoryImpl(AuthorMapper authorMapper){
        this.authorMapper = authorMapper;
    }

    @Override
    public Author selectById(Long id) {
        return this.authorMapper.selectById(id);
    }

    @Override
    public void insertEntity(Author entity) {
        this.authorMapper.insert(entity);
    }
}
