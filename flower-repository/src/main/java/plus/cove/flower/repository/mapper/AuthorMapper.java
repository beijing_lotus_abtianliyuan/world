package plus.cove.flower.repository.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import plus.cove.flower.domain.entity.author.Author;

/**
 * 作者仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Mapper
public interface AuthorMapper {
    Author selectById(Long id);

    void insert(@Param("et") Author entity);
}
