package plus.cove.flower.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import plus.cove.flower.domain.entity.global.District;
import plus.cove.flower.domain.repository.DistrictRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class DistrictRepositoryImpl implements DistrictRepository {
    private final Logger log = LoggerFactory.getLogger(DistrictRepositoryImpl.class);

    @Override
    public List<District> selectEntity(District entity) {
        return null;
    }

    @Override
    public void insertEntity(District entity) {
        return;
    }
}
