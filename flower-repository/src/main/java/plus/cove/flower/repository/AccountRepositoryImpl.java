package plus.cove.flower.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import plus.cove.flower.domain.entity.account.Account;
import plus.cove.flower.domain.repository.AccountRepository;
import plus.cove.flower.repository.mapper.AccountMapper;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class AccountRepositoryImpl implements AccountRepository {
    private final Logger log = LoggerFactory.getLogger(AccountRepositoryImpl.class);
    private AccountMapper accountMapper;

    public AccountRepositoryImpl(AccountMapper accountMapper){
        this.accountMapper = accountMapper;
    }

    @Override
    public Account selectByName(String name) {
        return accountMapper.selectByName(name);
    }

    @Override
    public void insertEntity(Account account) {
        accountMapper.insert(account);
    }
}
