package plus.cove.flower.repository.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import plus.cove.flower.domain.entity.account.Account;

/**
 * 账户仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Mapper
public interface AccountMapper {
    Account selectByName(String name);

    void insert(@Param("et") Account entity);
}
