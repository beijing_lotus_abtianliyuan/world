package plus.cove.flower.webapi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import plus.cove.flower.application.AuthorApplication;
import plus.cove.flower.application.view.author.AuthorMineOutput;
import plus.cove.flower.webapi.component.BaseController;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * 作者控制器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Path(value = "/api/author")
@Produces(MediaType.APPLICATION_JSON)
public class AuthorController extends BaseController {
    private final Logger log = LoggerFactory.getLogger(AuthorController.class);
    private final AuthorApplication authorApp;

    AuthorController(final AuthorApplication authorApp) {
        this.authorApp = authorApp;
    }

    @GET
    @Path("mine")
    public AuthorMineOutput getAuthor(Long authorId) {
        AuthorMineOutput author = authorApp.loadAuthor(authorId);
        return author;
    }
}
