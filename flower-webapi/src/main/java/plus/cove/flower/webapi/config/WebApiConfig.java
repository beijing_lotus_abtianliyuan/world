package plus.cove.flower.webapi.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.quarkus.jackson.ObjectMapperCustomizer;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Singleton;

/**
 * api配置
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class WebApiConfig {
    @Singleton
    ObjectMapper objectMapper(Instance<ObjectMapperCustomizer> customizers) {
        JsonMapper jsonMapper = JsonMapper.builder()
                // 忽略重复注册
                .configure(MapperFeature.IGNORE_DUPLICATE_MODULE_REGISTRATIONS, false)
                // 反序列化忽略不存在的属性
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                // 使用ISO-8601格式化日期和时间
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .build();

        SimpleModule simpleModule = new SimpleModule();
        // Long类型
        simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
        simpleModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
        jsonMapper.registerModule(simpleModule);

        for (ObjectMapperCustomizer customizer : customizers) {
            customizer.customize(jsonMapper);
        }

        return jsonMapper;
    }
}
