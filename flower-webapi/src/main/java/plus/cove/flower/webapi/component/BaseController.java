package plus.cove.flower.webapi.component;

import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * 控制器基类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class BaseController {
    private final Logger log = LoggerFactory.getLogger(BaseController.class);

    @Inject
    protected HttpServerRequest request;

    @Inject
    protected HttpServerResponse response;
}
