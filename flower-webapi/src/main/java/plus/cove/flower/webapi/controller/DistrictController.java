package plus.cove.flower.webapi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import plus.cove.flower.application.DistrictApplication;
import plus.cove.flower.application.view.district.DistrictQueryInput;
import plus.cove.flower.application.view.district.DistrictQueryOutput;
import plus.cove.flower.application.view.district.DistrictSaveInput;
import plus.cove.flower.domain.entity.global.District;
import plus.cove.flower.webapi.component.BaseController;
import plus.cove.infrastructure.component.ActionResult;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * 区域控制器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Path(value = "/api/district")
public class DistrictController extends BaseController {
    private final Logger log = LoggerFactory.getLogger(DistrictController.class);
    private final DistrictApplication districtApp;

    DistrictController(final DistrictApplication districtApp) {
        this.districtApp = districtApp;
    }

    @GET
    @Path("city")
    @Produces(MediaType.APPLICATION_JSON)
    public List<DistrictQueryOutput> getCity(@QueryParam("name") String name) {
        DistrictQueryInput input = DistrictQueryInput.from(name);
        List<DistrictQueryOutput> list = districtApp.loadDistrict(input);
        return list;
    }

    @POST
    @Path("city")
    @Produces(MediaType.APPLICATION_JSON)
    public ActionResult saveCity(@Valid DistrictSaveInput input) {
        District entity = input.toEntity();
        districtApp.saveDistrict(entity);
        return ActionResult.success();
    }
}
