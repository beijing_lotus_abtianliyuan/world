package plus.cove.flower.webapi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import plus.cove.flower.application.AuthorApplication;
import plus.cove.flower.application.view.author.AuthorLoginInput;
import plus.cove.flower.application.view.author.AuthorLoginOutput;
import plus.cove.flower.application.view.author.AuthorSignupInput;
import plus.cove.flower.application.view.author.AuthorSignupOutput;
import plus.cove.flower.webapi.component.BaseController;

import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * 用户控制器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Path(value = "/api/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountController extends BaseController {
    private final Logger log = LoggerFactory.getLogger(AccountController.class);
    private final AuthorApplication authorApp;

    AccountController(final AuthorApplication authorApp) {
        this.authorApp = authorApp;
    }

    @POST
    @Path("signup")
    public AuthorSignupOutput userSignup(@Valid AuthorSignupInput input) {
        AuthorSignupOutput output = authorApp.signupAuthor(input);
        return output;
    }

    @POST
    @Path("login")
    public AuthorLoginOutput userLogin(@Valid AuthorLoginInput input) {
        AuthorLoginOutput output = authorApp.loginAuthor(input);
        return output;
    }
}
