package plus.cove.flower.webapi.controller;

import plus.cove.flower.webapi.component.BaseController;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * 首页控制器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Path(value = "/")
public class HomeController extends BaseController {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String homeIndex() throws IOException {
        return "ok";
    }
}
