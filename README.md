# world

### 项目介绍
本系列文章使用Quarkus搭建一个通用的项目架构；
内容包括领域开发，主流技术，最佳实践，涉及开发，部署，监控；

### 主要技术
Java17+Quarkus2+JPA+Redis+Docker

### 主要目标
使用GraalVM+Reactive提高并发，降低内存；
提供底层无关的开发体验；

### 适用范围
- 小项目`百万级别`
- 小团队`十人团队`

### 开发环境
Windows10+IDEA+Docker

- 开发环境 mvn quarkus:dev
- 发布环境 project/build/deploy.sh | deploy.sh [profile] `直接部署到远程服务`
- 容器环境 project/build/docker.sh | docker.sh [profile] [version] `生成docker镜像`

### 分层介绍
Interface接口层-负责处理网络协议相关的逻辑
``` 
** 主要功能 **
1. 网络协议的转换，获取数据并转换为相应的Bean，一般有框架处理
2. 统一鉴权，Session管理，获取当前用户，并作鉴权和校验
3. 异常处理，避免异常直接暴露给调用端，接口层做统一异常捕获，转化为调用端可以理解的数据格式

规范1： 
Interface层的Http接口，返回值为ActionResult，捕获所有异常
```
Application应用层-负责业务流程
```dtd
** 主要功能 **
1. 业务编排，不负责业务逻辑
2. 领域模型与对外输出DTO进行转换，包括Query, Input, Event
3. 使用合适的仓储获取数据和持久化数据

规范1：建议接入参数只有一个对象（例外情况：根据单一ID查询和分页的情况）
规范2：针对不同的语义，要避免对象的复用

```

### 使用说明
[Quarkus入门系列](https://www.jianshu.com/p/d41df3c39647 "源码说明")

> 准备篇  
> 准备篇-Quarkus是什么
> 准备篇-使用GraalVM
> 准备篇-关于JavaEE
> 准备篇-与SpringBoot不同

> 基础篇  
> 基础篇-校验

> 实战篇  
> 实战篇-部署 
> 实战篇-监控



